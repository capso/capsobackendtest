<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title>Welcome to CapSo</title>
</head>
<body>
<h2>Introduction</h2>
<p>Thank you for your interest in CapSo's backend developer position. In this task, you will create a simple social network where people are connected by undirected links - i.e. friendship - and provide restful APIs to query social connections.</p>
<p>This repo provides a stub Grails application for you to start with as well as dummy data in the <code>resources</code> folder. We also accept Spring+Hibernate+H2/MySQL solution if you prefer</p>
<h3>Set up</h3>
<ul>
<li>You will need Java 7</li>
<li>Download <a href="https://grails.org/download.html">Grails 2.3.8</a></li>
<li>Install Grails 2.3.8 by setting the <code>PATH</code> and <code>GRAILS_HOME</code> as appropriate</li>
<li>Clone the repository to your local drive</li>
<li>In the project directory, run:
<code>grails RefreshDependencies &amp;&amp; grails run-app</code></li>
<li>The app is running at <a href="http://localhost:8080" rel="nofollow"  >http://localhost:8080</a></li>
</ul>
<h3 id="markdown-header-submissions">Submissions</h3>
<ul>
<li>Do not make a pull request</li>
<li>When you finish the task, please email the complete project folder to gyang@capso.com with title "Backend Test - &lt;your name&gt;"</li>
</ul>
<h3 id="markdown-header-data-description">Data description</h3>
<p>We provide dummy data for you to work with in the <code>resources</code> folder. <code>people.json</code> contains a list of people with <code>id</code> and <code>name</code>. <code>connection.json</code> contains a list of pairs of person IDs. You should populate the network with the dummy data on app startup.</p>
<h3 id="markdown-header-requirements">Requirements</h3>
<ol>
<li>
<p>Design domain classes to model the social networks</p>
</li>
<li>
<p>Restful APIs</p>
<ul>
<li><a href="http://localhost:8080/api/people" rel="nofollow"  >http://localhost:8080/api/people</a> <strong>MUST</strong> list all people in the system. </li>
<li><a href="http://localhost:8080/api/person/$id/friends" rel="nofollow"  >http://localhost:8080/api/person/$id/friends</a> <strong>MUST</strong> return all friends of the person</li>
<li><a href="http://localhost:8080/api/person/$fromid/distance/$toid" rel="nofollow"  >http://localhost:8080/api/person/$fromid/distance/$toid</a> <strong>SHOULD</strong> give the minimum number of people to introduce one</li>
<li><a href="http://localhost:8080/api/people/coolkid" rel="nofollow"  >http://localhost:8080/api/people/coolkid</a> <strong>SHOULD</strong> return the most connected person in the system and the number of connections he/she has</li>
<li>You <strong>COULD</strong> come up with more APIs to reveal further information of the social networks. Be bold and creative!</li>
</ul>
</li>
<li>
<p>All APIs <strong>SHOULD</strong> have automated tests</p>
</li>
<li>You <strong>SHOULD</strong> consider the scenario where there are millions of records in the system - how does your solution scale?</li>
</ol>
</body>
</html>
