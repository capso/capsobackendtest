class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
		group("/api"){
			"/person/$id/friends" (controller: 'person', action:'friends', namespace:'api')
			"/person/$fromid/distance/$toid" (controller: 'person', action:'distance', namespace:'api')
			"/people/coolkid" (controller: 'person', action:'mostConnected', namespace: 'api')
			"/people" (controller: 'person', action:'index', namespace:'api')
        }
		
	}
}
