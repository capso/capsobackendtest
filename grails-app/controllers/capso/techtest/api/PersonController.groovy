package capso.techtest.api

import grails.converters.JSON

class PersonController {
	static namespace = 'api'
	
	
	
    /**
     * List of all people
     * @return
     */
    def index() {
		def responseData = [
			"Your output here"
		]
		render responseData as JSON
	}
	
	/**
	 * List of all friends of the person
	 * @param id of the person
	 * @return
	 */
	def friends(Long id){
		def responseData = [
			"Your output here"
		]
		render responseData as JSON
	}
	
	/**
	 * degree of separation between two people
	 */
	def distance(Long fromid, Long toid){
		def responseData = [
			"Your output here"
		]
		
		render responseData as JSON
	}
	
	/**
	 * Gives the person with the most friends
	 */
	def mostConnected(){
		def responseData = [
			"Your output here"
		]
		render responseData as JSON
	}
	
}
