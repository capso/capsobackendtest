# Introduction #

Thank you for your interest in CapSo's backend developer position. In this task, you will create a simple social network where people are connected by undirected links - i.e. friendship - and provide restful APIs to query social connections.

This repo provides a stub Grails application for you to start with as well as dummy data in the `resources` folder. We also accept Spring+Hibernate+H2/MySQL solution if you prefer

### Set up ###
* You will need Java 7
* Download [Grails 2.3.8](https://grails.org/download.html)
* Install Grails 2.3.8 by setting the `PATH` and `GRAILS_HOME` as appropriate
* Clone the repository to your local drive
* In the project directory, run:
`
grails RefreshDependencies && grails run-app
`
* The app is running at http://localhost:8080

### Submissions ###

* Do not make a pull request
* When you finish the task, please email the complete project folder to gyang@capso.com with title "Backend Test - <your name>"

### Data description ###
We provide dummy data for you to work with in the `resources` folder. `people.json` contains a list of people with `id` and `name`. `connection.json` contains a list of pairs of person IDs. You should populate the network with the dummy data on app startup.

### Requirements ###

1. Design domain classes to model the social networks

2. Restful APIs
    * http://localhost:8080/api/people **MUST** list all people in the system. 
    * http://localhost:8080/api/person/$id/friends **MUST** return all friends of the person
    * http://localhost:8080/api/person/$fromid/distance/$toid **SHOULD** give the minimum number of people to introduce one
    * http://localhost:8080/api/people/coolkid **SHOULD** return the most connected person in the system and the number of connections he/she has
    * You **COULD** come up with more APIs to reveal further information of the social networks. Be bold and creative!

3. All APIs **SHOULD** have automated tests
4. You **SHOULD** consider the scenario where there are millions of records in the system - how does your solution scale?
